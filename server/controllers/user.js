const { User } = require('../models/User');
const { getAccountkitData } = require('../services/facebookAccountkit')
const { SiteSettings } = require('../models/SiteSettings')




exports.register = function (req, res) {

    let { email, password, username } = req.body;
    let role = req.body.role ? parseInt(req.body.role) : 1;
    let root = req.body.root || false;
    let siteId = req.body.siteId;



    if (root) {
        user = new User({

            local: {
                username: username,
                email: email,
                password: password,

            },

            role: role,
            root: root

        })
    } else {
        user = new User({

            local: {
                username: username,
                email: email,
                password: password,

            },

            role: role,
            root: root,
            siteId: siteId

        })
    }








    User.findOne({ 'local.email': email }, (err, userFound) => {


        let errors = {};

        if (userFound) {
            errors['email'] = 'email is already in used';
            return res.status(500).json({
                success: false,
                errors: errors
            })


        } else {
            user.save((err, newUser) => {
                res.status(200).send({
                    success: true
                })
            })

        }

    })




}



exports.accountkitLogin = function (req, res) {



    // call accountkit service function and get phonenumber and access token


    getAccountkitData(req.body.code, (phone) => {




        User.findOne({ 'facebook.email': phone }, (err, user) => {

            if (!user) {

                const user = new User({

                    facebook: {
                        email: phone

                    }

                })

                user.save((err, userDoc) => {
                    if (err) { return res.json({ success: false, errors: true }) }

                    // res.status(200).json({ success: true })
                    userDoc.generateToken((err, user) => {


                        if (err) {
                            return res.status(400).send(err)
                        }


                        res.send({ loginSuccess: true, token: user.token })

                    })
                })
            } else {
                res.send({ loginSuccess: true, token: user.token })
            }









        })





    })




}


exports.login = function (req, res) {

    let errors = {};


    User.findOne({ 'local.email': req.body.email }, (err, user) => {


        if (!user) {
            errors.nouser = 'there is no user found with email'
            res.status(500).send({
                success: false,
                errors: errors

            })

        } else {

            user.comparePassword(req.body.password, (err, isMatch) => {
                if (!isMatch) {
                    errors.wrongpass = 'wrong password. try again'
                    res.status(500).send({
                        success: false,
                        errors: errors

                    })
                } else {

                    user.generateToken((err, user) => {
                        if (err) { return res.status(400).send(err) } else {

                            res.send({ success: true, token: user.token })
                        }
                    })

                }




            })





        }




    })



}

exports.auth = function (req, res) {


    if (!req.user.facebook) {

        let isAdmin = req.user.role === 2 ? true : false
        let isEditor = req.user.role === 1 ? true : false
        res.status(200).json({
            email: req.user.local.email,
            username: req.user.local.username,
            isAuth: true,
            isAdmin: isAdmin,
            isEditor,
            authType: 'local',
            _id: req.user._id



        })

    } else {
        let isAdmin = req.user.role === 2 ? true : false
        let isEditor = req.user.role === 1 ? true : false

        res.status(200).json({
            email: req.user.facebook.email,
            username: req.user.facebook.username || 'phone user',
            isAuth: true,
            isAdmin: isAdmin,
            isEditor: isEditor,
            authType: 'facebook',
            _id: req.user._id




        })
    }





}


exports.logout = function (req, res) {

    User.findOneAndUpdate({ _id: req.user._id }, { token: '' }, (err, updateUser) => {


        if (err) { return res.json({ success: false, err }) }
        return res.status(200).send({
            success: true
        })


    })




}