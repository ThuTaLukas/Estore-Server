

const { SiteSettings } = require('../../models/SiteSettings')


const getSiteSettings = (req, res) => {


    SiteSettings.findOne({}, (err, site) => {


        if (err) {

            res.status(500).send({
                success: false,
                errors: { message: err.message }
            })


        } else {

            res.status(200).send({
                success: true,
                site: site
            })
        }
    })




}





module.exports = {
    getSiteSettings

}