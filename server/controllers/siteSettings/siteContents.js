const { SiteContents } = require('../../models/SiteContents')
const uuidv4 = require('uuid/v4');


const deleteFAQ = (req, res) => {


    const { id, siteContentId } = req.body
    SiteContents.findOneAndUpdate({ _id: siteContentId }, { $pull: { 'faqs': { id: id } } }, { new: true }, (err, updated) => {
        res.status(200).send({
            success: true
        })
    })


}

const deleteSlider = (req, res) => {


    const { id, siteContentId } = req.body
    SiteContents.findOneAndUpdate({ _id: siteContentId }, { $pull: { 'headerImages': { id: id } } }, { new: true }, (err, updated) => {
        res.status(200).send({
            success: true
        })
    })


}

const getSliders = (req, res) => {

    SiteContents.findOne({}, (err, siteContents) => {

        if (siteContents) {
            res.status(200).send({

                sliders: siteContents ? siteContents.headerImages : [],
                id: siteContents._id
            })

        } else {
            res.status(500).send({

                success: false
            })
        }

    })

}
const getFAQS = (req, res) => {

    SiteContents.findOne({}, (err, siteContents) => {


        if (siteContents) {

            res.status(200).send({

                faqs: siteContents ? siteContents.faqs : [],
                id: siteContents._id
            })

        } else {
            res.status(500).send({

                success: false
            })
        }


    })

}

const createSlider = (req, res) => {

    const { slider, siteContentId } = req.body;


    const finalSlider = {
        ...slider,
        id: uuidv4()
    }

    if (siteContentId) {


        SiteContents.findOneAndUpdate({ _id: siteContentId }, { $push: { headerImages: finalSlider } }, { new: true, upsert: true }, (err, content) => {
            res.status(200).send({
                success: true,
                sliders: content.headerImages
            })
        })


    } else {

        let siteContent = new SiteContents({
            headerImages: [slider]
        })

        siteContent.save((err, content) => {

            res.status(200).send({
                success: true,
                sliders: content.headerImages
            })

        })
    }




}

const createFAQ = (req, res) => {

    const { faq, siteContentId } = req.body;

    const finalFAQ = {
        ...faq,
        id: uuidv4()
    }

    if (siteContentId) {


        SiteContents.findOneAndUpdate({ _id: siteContentId }, { $push: { faqs: finalFAQ } }, { new: true, upsert: true }, (err, content) => {
            res.status(200).send({
                success: true,
                faqs: content.faqs
            })
        })


    } else {

        let siteContent = new SiteContents({
            faqs: [faq]
        })

        siteContent.save((err, content) => {

            res.status(200).send({
                success: true,
                faqs: content.faqs
            })

        })
    }




}

module.exports = {
    createSlider,
    getSliders,
    createFAQ,
    getFAQS,
    deleteSlider,
    deleteFAQ
}