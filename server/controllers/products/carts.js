
const { User } = require('../../models/User')
const { Cart } = require('../../models/Cart')
const { Promotion } = require('../../models/Promotion')

const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;


exports.removeCartItem = function (req, res) {

    const { itemId, cartId } = req.params;

    Cart.update({ _id: cartId }, { $pull: { 'items': { _id: itemId } } }, (err, updatedCart) => {

        if (err) {
            console.log(err)
        }
        res.status(200).send({
            success: true
        })
    })

}

exports.getUserCart = function (req, res) {

    let errors = {}
    const { cartId, promoId } = req.body;

    if (cartId) {

        Cart.aggregate(
            [
                { $match: { _id: ObjectId(cartId) } },
                {
                    $project: {
                        "totalPrice": {
                            $reduce: {
                                input: "$items",
                                initialValue: 0,
                                in: { $sum: "$items.TotalPrice" }
                            }
                        },
                        "totalQuantity": {
                            $reduce: {
                                input: "$items",
                                initialValue: 0,
                                in: { $sum: "$items.quantity" }
                            }
                        },

                        "items": 1


                    }
                }

            ] ,


        ).exec(async (err, results) => {


            let percent = 100;

            // if user apply promotion code , search promotion and if true , define percent
            if (promoId) {

                // have to wait , otherwise , percent will be only 100 since it skip and don't wait callback
                await Promotion.findOne({ name: promoId }, (err, promo) => {

                    if (promo) {
                        console.log('happen')
                        if (promo.quantity > 0) {

                            percent = promo.percent;

                            Promotion.updateOne({ name: promoId }, { $inc: { quantity: -1 } }, { new: true }, (err, updatedPromo) => {

                            })


                        }
                        if (promo.quantity === 0) {
                            errors.promotionSold = 'promo code is out of stock. sorry'

                            res.status(400).json({
                                success: false,
                                errors: errors
                            })
                        }



                    } else {

                        errors.promotionNotFound = 'promo code is invalid'

                        res.status(500).json({
                            success: false,
                            errors: errors
                        })

                    }


                })


            }



            let totalPriceAfterPromo = results[0].totalPrice * (percent / 100)
            // updating Customer 's cart with total Price and totalQuantity for all products
            Cart.updateOne({ _id: cartId }, { $set: { totalPrice: totalPriceAfterPromo, totalQuantity: results[0].totalQuantity } }, { new: true }, (err, updatedCart) => {

            })

            // sending new result excluding promotion percent - to use in cart component
            let newResult = {
                ...results[0],
                totalPrice: totalPriceAfterPromo
            }


            res.status(200).json({
                results: newResult
            })



        })

    } else {
        console.log('nothing to do without cartId');
    }





}

exports.addToCart = function (req, res) {

    const { cartItem, cartId } = req.body;

    Cart.findOne({ _id: cartId }, (err, cart) => {

        if (err) {
            console.log(err);
        }

        if (!cart) {

            console.log('new');
            const newCart = new Cart({
                totalPrice: cartItem.TotalPrice,
                totalQuantity: cartItem.quantity,
                items: [cartItem]

            })

            newCart.save((err, cartItem) => {
                res.status(200).send({
                    success: true,
                    cartId: cartItem._id

                })


            })
        } else {


            Cart.updateOne({ _id: cartId }, { $push: { items: cartItem } }, { new: true }, (err, updatedCart) => {
                res.status(200).send({
                    success: true

                })
            })


        }
    })




}