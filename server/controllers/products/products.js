const { Product } = require('../../models/Product');
const { Category } = require('../../models/Category')
const { Brand } = require('../../models/Brand')
const { Variant } = require('../../models/Variant')
const { Collection } = require('../../models/Collection')





exports.deleteCollection = function (req, res) {

    const { collectionId } = req.params;



    Collection.findByIdAndDelete(collectionId, (err, deletedCollection) => {

        if (err) {
            console.log(err)
        }

        res.status(200).send({
            success: true
        })

    })




}
exports.deleteVariant = function (req, res) {

    const { variantId } = req.params;

    Variant.findByIdAndDelete(variantId, (err, deletedVariant) => {

        if (err) {
            console.log(err)
        }

        res.status(200).send({
            success: true
        })

    })




}



exports.addVariant = function (req, res) {


    const { name, id, image } = req.body;


    const newVariant = new Variant({
        name,
        id,
        image: image || 'https://via.placeholder.com/200',
        creator: req.user._id

    })

    newVariant.save((err, variant) => {

        if (err) {

            res.status(500).send({
                success: false
            })

        } else {
            res.status(200).send({
                success: true,
                variant: variant
            })

        }
    });
}


exports.getVariants = function (req, res) {

    Variant.find({}, (err, variants) => {

        if (err) {

            res.status(500).send({
                success: false
            })




        } else {


            res.status(200).send({
                success: true,
                variants: variants
            })


        }

    })

}


exports.addCollection = function (req, res) {

    const { name, id, image } = req.body;
    const errors = {}

    const newCollection = new Collection({
        name,
        id,
        image: image || 'https://via.placeholder.com/728x210',
        creator: req.user._id

    })

    newCollection.save((err, collection) => {

        if (err) {

            res.status(500).send({
                success: false
            })

        } else {
            res.status(200).send({
                success: true,
                collection: collection
            })

        }
    });



}


exports.getCollections = function (req, res) {


    Collection.find({}, (err, collections) => {

        if (err) {

            res.status(500).send({
                success: false
            })




        } else {


            res.status(200).send({
                success: true,
                collections: collections
            })


        }

    })


}






exports.addLike = function (req, res) {



    const { productId } = req.body;


    Product.findByIdAndUpdate(productId, { $inc: { likes: 1 } }, { new: true }, (err, updatedProduct) => {
        res.status(200).send({
            success: true
        })
    })



}

exports.getSingleProduct = function (req, res) {

    const { productId } = req.params;


    Product.findById(productId, (err, product) => {

        if (err) {
            console.log(err);
        }
        if (!product) {
            res.status(500).send({
                success: false
            })
        }
        res.status(200).send({
            product: product,
            success: true
        })

    })

}




exports.getProductsByCat = function (req, res) {


    let { catId } = req.params;

    Product.find({
        category: catId
    }, (err, products) => {


        if (err) {
            res.status(400).send({ message: 'error in querying products by category' })


        } else {





            res.status(200).send({
                success: true,
                products: products
            })
        }



    })



}

exports.getProductsShop = function (req, res) {

    let { filters, name } = req.body;
    let sortBy = req.body.sortBy ? req.body.sortBy : "createdAt";
    let ITEMS_PER_PAGE = 20;
    let page = req.body.page || 1;
    let totalItems;
    let findArgs;


    if (name) {
        findArgs = {

            $text: { $search: name }

        }


    } else {


        findArgs = {

        }


        // dynamic filtering
        for (let key in req.body.filters) {


            if (req.body.filters[key].length > 0) {


                switch (key) {
                    case 'price':
                        findArgs['price.normal'] = {
                            $gte: parseInt(req.body.filters[key][0]),
                            $lte: parseInt(req.body.filters[key][1])
                        }
                        break;
                    case 'collections':
                        findArgs[key] = {
                            $in: req.body.filters[key]
                        }
                        break;

                    default:
                        findArgs[key] = req.body.filters[key]
                        break;
                }









            }
        }


    }




    Product
        .countDocuments()
        .then((count) => {

            totalItems = count
            return Product
                .find(findArgs)
                .populate('category')
                .populate('brand')
                .populate('collections')
                .sort([[sortBy, -1]])
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

        })
        .then((products) => {


            res.status(200).json({
                success: true,
                totalItems: totalItems,
                products: products,
                currentPage: page,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                hasPreviousPage: page > 1,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems

            })

        })



}


exports.getBrands = function (req, res) {

    Brand.find({}, (err, brands) => {


        if (err) {

            console.log(err)

        }

        res.status(200).send({
            success: true,
            brands: brands
        })
    })


}


exports.addBrand = async function (req, res) {


    const { name, id, image } = req.body;


    try {


        const brand = new Brand({
            name: name,
            id: id,
            image: image || 'https://via.placeholder.com/200',
            creator: req.user._id

        })

        await brand.save();
        res.status(200).send({ success: true, brand })

    } catch (error) {
        console.log(error);

        res.status(500).send({
            error: 'Error occurs during adding brand'
        })
    }


}



exports.deleteBrand = function (req, res) {

    const { brandId } = req.params;



    Brand.findByIdAndDelete(brandId, (err, deletedBrand) => {

        if (err) {
            console.log(err)
        }

        res.status(200).send({
            success: true
        })

    })


}
exports.editProduct = function (req, res) {


    const { productId } = req.params;



    Product.findOneAndUpdate({ _id: productId }, { $set: req.body }, { new: true }, (err, product) => {



        if (err) {
            res.status(500).send({ error: err, message: 'problem in updating product' })

        } else {

            res.status(200).send({ success: true, product: product })
        }

    })


}


exports.deleteProduct = function (req, res) {


    let { productId } = req.params;

    Product.findByIdAndDelete(productId, (err, deletedProduct) => {
        res.status(200).send({
            success: true
        })
    })

}





exports.getProducts = function (req, res) {

    const { filters } = req.body;
    let ITEMS_PER_PAGE = 20;
    let page = req.body.page || 1;
    let totalItems;
    let findArgs;

    // TO-DO filters arrays and use it
    if (filters && filters.namesku) {
        findArgs = {

            $text: { $search: filters.namesku }

        }

    } else {

        findArgs = {};

        for (let key in filters) {


            if (filters.hasOwnProperty(key)) {

                const element = filters[key];

                switch (key) {
                    case 'price':
                        findArgs['price.normal'] = {
                            $gt: parseInt(element.from), $lt: parseInt(element.to)
                        }
                        break;
                    default:
                        break;
                }

            }
        }


    }

    Product
        .countDocuments()
        .then((count) => {

            totalItems = count
            return Product
                .find(findArgs)
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

        })
        .then((products) => {


            res.status(200).json({
                success: true,
                totalItems: totalItems,
                products: products,
                currentPage: page,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                hasPreviousPage: page > 1,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems

            })

        })

}

exports.deleteCategory = function (req, res) {

    const { catId } = req.params;



    Category.findByIdAndDelete(catId, (err, deletedCategory) => {

        if (err) {
            console.log(err)
        }

        res.status(200).send({
            success: true
        })

    })


}



exports.getCategories = function (req, res) {



    Category.find({}, (err, categories) => {


        if (err) {

            console.log(err)

        }

        res.status(200).send({
            success: true,
            categories: categories
        })
    })

}


exports.addCategory = async function (req, res) {


    const { name, id, image } = req.body;


    try {

        const category = new Category({
            name: name,
            id: id,
            image: image || 'https://via.placeholder.com/200',
            creator: req.user._id

        })

        await category.save();
        res.status(200).send({ success: true, category })

    } catch (error) {
        console.log(error);

        res.status(500).send({
            error: 'Error occurs during adding category'
        })
    }


}

exports.addProduct = async function (req, res) {

    // deconstruct  product's variables
    const { name, price, sku,
        category, description, brand,
        stocks, likes, images, variants, collections,
        creator, details
    } = req.body;


    // add product data into Product model 
    try {

        const product = new Product({
            name,
            price,
            sku,
            category,
            brand,
            description,
            stocks,
            likes,
            variants,
            details,
            images,
            collections: collections,
            creator: req.user._id
        });


        await product.save();
        res.status(200).send({
            success: true,
            product: product
        })

    } catch (error) {
        console.log(error)

        res.status(500).send({
            message: 'Error occurs during adding product',
            success: false
        })

    }








}



// exports.deleteProduct = function (req, res) {


// }

