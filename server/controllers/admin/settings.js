const { SiteSettings } = require('../../models/SiteSettings')





exports.getStoreProfile = function (req, res) {

    let siteId = req.user.root ? req.user._id : req.user.siteId
    let errors = {}


    SiteSettings.findOne({ siteId: siteId }, (err, siteProfile) => {


        if (err) {

            errors.notFound = 'error in getting store profile'
            res.status(500).send({
                success: false,
                errors: errors

            })



        } else {

            res.status(200).send({
                success: true,
                profile: siteProfile
            })


        }


    })



}




exports.CUStoreInfo = function (req, res) {



    let errors = {}
    // if root is true >> find SiteSettings by user _id >> 


    if (req.user.root) {

        SiteSettings.findOne({ siteId: req.user._id }, (err, sitesetting) => {


            //   if SiteSetting exist >> update SiteSetting
            if (sitesetting) {


                SiteSettings.updateOne({ siteId: req.user._id }, { $set: req.body }, { new: true }, (err, updatedSiteSetting) => {

                    res.send({
                        updateSuccess: true
                    })
                })


            } else {
                //   if SiteSetting not >> create new SiteSetting

                const storeInfo = new SiteSettings({
                    ...req.body,
                    siteId: req.user._id
                })

                storeInfo.save((err, info) => {

                    res.send({
                        success: true
                    })

                })


            }


        })




    } else {
        // if root is false >> find SiteSettings by siteId >> 
        // update SiteSettings

        SiteSettings.findOne({ siteId: req.user.siteId }, (err, sitesetting) => {

            if (err) {

                errors.notFound = 'there is no sitesetting from store owner. u cant do it'
                res.status(500).send({
                    success: false,
                    errors: errors

                })


            } else {

                SiteSettings.updateOne({ siteId: req.user.siteId }, { $set: { store: req.body.store } }, { new: true }, (err, updatedSiteSetting) => {

                    res.send({
                        updateSuccess: true
                    })
                })




            }


        })


    }




}