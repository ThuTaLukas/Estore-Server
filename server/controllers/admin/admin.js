const { User } = require('../../models/User')
const { Order } = require('../../models/Order')
const { Promotion } = require('../../models/Promotion')



exports.editUser = function (req, res) {


    const { userId, user } = req.body;
    let errors = {}
    let userToEdit = {
        local: {}
    }


    for (let item in user) {

        const element = user[item];
        if (item === 'role') {

            userToEdit['role'] = parseInt(element)
        } else {
            userToEdit.local[item] = element
        }


    }

    console.log(user)


    User.findOneAndUpdate({ _id: userId }, { $set: userToEdit }, { new: true }, (err, updatedUser) => {

        console.log(updatedUser)
        if (err) {

            errors.notFound = 'user not found to update'
            res.status(500).send({
                success: false,
                errors: errors

            })

        } else {
            res.status(200).send({
                success: true,

            })
        }





    })

}

exports.deleteUser = function (req, res) {


    const { userId } = req.params;

    let errors = {}


    User.findByIdAndDelete(userId, (err) => {


        if (err) {
            errors.notDelete = 'cannot delete. something wrong'
            res.status(500).send({
                success: false,
                errors: errors

            })




        } else {
            res.status(200).send({
                success: true
            })
        }


    })



}



exports.editPromotion = function (req, res) {


    const { promoId, promotion } = req.body;
    let errors = {}


    Promotion.findByIdAndUpdate(promoId, { $set: promotion }, { new: true }, (err, updatedPromo) => {


        if (err) {

            errors.notFound = 'promotion not found to update'
            res.status(500).send({
                success: false,
                errors: errors

            })

        }


        res.status(200).send({
            success: true,

        })


    })




}

exports.getSingleUser = function (req, res) {


    const { userId } = req.params;

    let errors = {}


    User.findById(userId, (err, user) => {


        if (err) {
            errors.notFound = 'user not found.'
            res.status(500).send({
                success: false,
                errors: errors

            })
        }


        // modify user object before sending

        let userModified = {
            username: user.local.username,
            email: user.local.email,
            password: '',
            role: user.role,
            _id: user._id
        }
        res.status(200).send({
            success: true,
            user: userModified
        })



    })





}

exports.getSinglePromotion = function (req, res) {


    const { promoId } = req.params;

    let errors = {}


    Promotion.findById(promoId, (err, promotion) => {


        if (err) {
            errors.notFound = 'promotion not found.'
            res.status(500).send({
                success: false,
                errors: errors

            })
        }

        res.status(200).send({
            success: true,
            promotion: promotion
        })



    })





}

exports.deletePromotion = function (req, res) {


    const { promoId } = req.params
    let errors = {}


    Promotion.findByIdAndDelete(promoId, (err) => {


        if (err) {
            errors.notDelete = 'cannot delete. something wrong'
            res.status(500).send({
                success: false,
                errors: errors

            })
        }

        res.status(200).send({
            success: true
        })
    })





}



exports.getPromotions = function (req, res) {

    let sortBy = req.body.sortBy ? req.body.sortBy : "createdAt";
    let ITEMS_PER_PAGE = req.body.limit ? parseInt(req.body.limit) : 10;
    let page = req.body.page || 1;
    let totalItems;
    let findArgs = {}




    // dynamic filtering
    for (let key in req.body.filters) {



        if (req.body.filters[key].length > 0) {
            // if (key === 'price') {

            //     findArgs[key] = {
            //         $gte: req.body.filters[key][0],
            //         $lte: req.body.filters[key][1]
            //     }

            // }

            // if (key === 'details') {
            //     req.body.filter[key].forEach((item) => {
            //         // TO-TEST
            //         return findArgs[key][item] = true
            //     })
            // }

            findArgs[key] = req.body.filters[key]

        }
    }




    Promotion
        .countDocuments()
        .then((count) => {

            totalItems = count
            return Promotion
                .find(findArgs)
                .sort([[sortBy, -1]])
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

        })
        .then((promotions) => {


            res.status(200).json({
                success: true,
                totalItems: totalItems,
                promotions: promotions,
                currentPage: page,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                hasPreviousPage: page > 1,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems

            })

        })




}



exports.addPromo = function (req, res) {

    let { name, percent, quantity } = req.body;

    percent = parseInt(percent)
    quantity = parseInt(quantity)

    const promotion = new Promotion({
        name: name,
        percent: percent,
        quantity: quantity
    })

    promotion.save((err, promotion) => {


        res.status(200).send({
            success: true,
            promotion: promotion
        })

    })



}



exports.getOrderById = function (req, res) {


    const { id } = req.params;
    let errors = {}



    Order.findById(id)
        .populate('localDelivery')
        .exec((err, order) => {

            if (!order) {
                errors.orderNotFound = 'order u find not exist'
                return res.status(500).send({
                    success: false,
                    errors: errors
                })
            } else {
                return res.status(200).send({
                    success: true,
                    order: order
                })
            }

        })









}

exports.getCustomers = function (req, res) {

    let totalItems;
    let { filters } = req.body;
    let ITEMS_PER_PAGE = req.body.limit ? parseInt(req.body.limit) : 10;
    let sortBy = req.body.sortBy ? req.body.sortBy : "createdAt";
    let page = req.body.page || 1;
    let findArgs = {}


    // only admins or editors
    findArgs.role = 0



    User.countDocuments()
        .then((count) => {

            totalItems = count

            return User.find(findArgs)
                .select('-token')
                .sort([[sortBy, -1]])
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

        })
        .then((customers) => {

            res.status(200).send({
                success: true,
                totalItems: customers.length,
                customers: customers,
                currentPage: page,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                hasPreviousPage: page > 1,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems

            })

        })







}

exports.getUsers = function (req, res) {

    let totalItems;
    let { filters } = req.body;
    let ITEMS_PER_PAGE = req.body.limit ? parseInt(req.body.limit) : 10;
    let sortBy = req.body.sortBy ? req.body.sortBy : "createdAt";
    let page = req.body.page || 1;
    let findArgs = {}


    // only admins or editors
    findArgs.role = { $gt: 0 }




    User.countDocuments()
        .then((count) => {

            totalItems = count

            return User.find(findArgs)
                .select('-token')
                .sort([[sortBy, -1]])
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

        })
        .then((users) => {

            res.status(200).send({
                success: true,
                totalItems: users.length,
                users: users,
                currentPage: page,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                hasPreviousPage: page > 1,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems

            })

        })






}