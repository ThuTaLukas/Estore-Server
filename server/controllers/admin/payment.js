
const { SiteSettings } = require('../../models/SiteSettings')


exports.addPayment = function (req, res) {

    /* 
    payment - payment object coming from req body 
    siteId - _id of sitesetting model

    */

    let errors = {};

    const { payment, siteId, edit } = req.body;


    SiteSettings.findById(siteId, (err, sitesetting) => {


        if (sitesetting) {

            /* 
            TO-DO  future
            checking that payment already exist in payments array
                    loop sitesetting.payments array . find if that payment name exist 
                        if exist . throw 
                        not . procced

            */


            if (edit) {
                SiteSettings.updateOne({ _id: siteId }, { $set: { payments: payment } }, { new: true }, (err) => {

                    res.status(200).send({
                        success: true
                    })
                })
            } else {
                SiteSettings.updateOne({ _id: siteId }, { $push: { payments: payment } }, { new: true }, (err) => {

                    res.status(200).send({
                        success: true
                    })
                })
            }



        } else {





            errors.notSiteSettings = 'Site is not created yet. go storeprofile and create first.'
            res.status(500).send({
                success: false,
                errors: errors

            })


        }


    })







}