const { Delivery } = require('../../models/Delivery')



exports.deleteDelivery = function (req, res) {

    const { deliveryId } = req.body;

    Delivery.findByIdAndDelete(deliveryId, (err) => {

        res.status(200).send({
            success: true
        })
    })


}

exports.updateDelivery = function (req, res) {

    const { delivery, deliveryId } = req.body


    let deliveryToUpdate = {
        township: delivery.township,
        town: delivery.town,
        price: delivery.price
    }

    Delivery.findByIdAndUpdate(deliveryId, { $set: deliveryToUpdate }, { new: true }, (err, updatedDeli) => {

        res.status(200).send({
            success: true
        })
    })


}


exports.editDelivery = function (req, res) {

    let errors = {};


    Delivery.findById(req.body.deliveryId, (err, delivery) => {


        if (delivery) {

            res.status(200).send({
                success: true,
                delivery: delivery
            })



        } else {

            errors.notFoundDelivery = 'Delivery not found'
            res.status(500).send({
                success: false,
                errors: errors

            })
        }
    })


}

exports.addDelivery = function (req, res) {


    const { town, township, price } = req.body


    const newDelivery = new Delivery({

        township,
        town,
        price,
        creator: req.user._id
    })



    Delivery.findOne({ town: town, township: township }, (err, delivery) => {


        if (err) {
            console.log(err);
        }

        if (!delivery) {


            newDelivery.save((err, addedDelivery) => {


                res.status(200).send({
                    success: true,
                    delivery: addedDelivery
                })


            })

        } else {

            res.send({ error: 'delivery is already existed . try to edit' })

        }



    })




}




exports.getDelivery = function (req, res) {


    Delivery.find({}, (err, deliveries) => {



        if (err) {
            console.log(err);
        }



        res.status(200).send({
            success: true,
            deliveries
        })




    })




}