const { User } = require('../../models/User')
const { Enquiry } = require('../../models/Enquiry')


const getEnquires = (req, res) => {

    Enquiry.find({}, (err, enquries) => {

        res.status(200).send({
            enquries
        })



    })

}




const sendEnquiry = (req, res) => {



    const { name, phone, description } = req.body
    const newEnquiry = new Enquiry({
        name: name || 'no name',
        phone: phone || 'no phone',
        description: description || 'no description'
    })

    newEnquiry.save((err, enquiry) => {

        res.status(200).send({
            success: true
        })
    })



}



module.exports = {

    sendEnquiry,
    getEnquires

}