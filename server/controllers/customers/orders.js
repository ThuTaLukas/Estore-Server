const { Order } = require('../../models/Order');
const { Cart } = require('../../models/Cart')
const { User } = require('../../models/User')
const moment = require('moment')

exports.changeStatus = async function (req, res) {


    const { orderId, status } = req.body;

    Order.findByIdAndUpdate(orderId, { $set: { status: status } }, { new: true }, (err, updated) => {
        res.status(200).send({
            success: true
        })
    })

}

exports.addOrder = async function (req, res) {


    let {
        cart, payment, shippingAddress, desireDate, localDelivery,

    } = req.body;

    let errors = {}

    try {

        const newOrder = new Order({
            cart,
            payment,
            shippingAddress,
            desireDate,
            localDelivery,
            customer: req.user._id

        })


        const order = await newOrder.save();
        await Cart.findOneAndDelete({ _id: order.cart._id })
        await User.findOneAndUpdate({ _id: req.user._id }, { $push: { 'orders': order._id } })

        res.status(200).send({
            success: true
        })

    } catch (error) {

        console.log(error.message)
        // erorrs.createOrder = 'something wrong while submitting order'
        // res.status(500).send({
        //     success: false,
        //     errors: erorrs
        // })
    }


}





isEmpty = (value) => {

    return value === null ||
        value === undefined ||
        (typeof value === 'object' && Object.keys(value).length === 0) ||
        (typeof value === 'string' && value.trim().length === 0)


}

exports.getOrdersByCustomer = function (req, res) {



    let sortBy = 'shipping.orderedDate'
    let order = req.query['order'] || -1;
    let ITEMS_PER_PAGE = 15;
    let findArgs = {};
    let totalItems;
    let queryParsed = JSON.parse(req.query.data)
    let page = parseInt(queryParsed.page) || 1;




    // if filters are not empty , time to filters
    if (!isEmpty(queryParsed.filters)) {

        let parseFilters = JSON.parse(req.query.data).filters;
        const startTime = moment(parseFilters.shipping['orderedDate']).startOf('day').toDate();
        const endTime = moment(parseFilters.shipping['orderDate']).endOf('day').toDate();

        findArgs = {
            'shipping.orderedDate': { $gte: startTime, $lt: endTime }
        }

    }


    Order.countDocuments()
        .then((totalCount) => {

            totalItems = totalCount;

            Order.find({
                customer: req.user._id,
                ...findArgs
            })
                .sort([[sortBy, order]])
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

                .exec((err, userOrders) => {

                    res.status(200).send({
                        orders: userOrders,
                        totalItems: totalItems,
                        success: true,
                        lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                        currentPage: page,
                        hasPreviousPage: page > 1,
                        hasNextPage: ITEMS_PER_PAGE * page < totalItems,
                        totalItems: totalItems
                    })
                })

        })










}

exports.getOrders = function (req, res) {



    const { filters } = req.body;
    let ITEMS_PER_PAGE = 20;
    let page = req.body.page || 1;
    let totalItems;
    let findArgs;

    if (!isEmpty(filters)) {

        findArgs = {}
        for (let key in filters) {




            if (filters.hasOwnProperty(key)) {

                let element = filters[key];

                switch (key) {

                    case 'desireDate':
                        findArgs[key] = {
                            $gte: moment(element).startOf('day').toDate(), $lt: moment(element).endOf('day').toDate()
                        }

                        break;
                    case 'orderedDate':
                        findArgs[key] = {
                            $gte: moment(element).startOf('day').toDate(), $lt: moment(element).endOf('day').toDate()
                        }
                        break;

                    default:
                        findArgs[key] = element
                        break;
                }

            }
        }


    }



    console.log(findArgs)




    Order
        .countDocuments()
        .then((count) => {

            totalItems = count
            return Order
                .find(findArgs)
                .skip((page - 1) * ITEMS_PER_PAGE)
                .limit(ITEMS_PER_PAGE)

        })
        .then((orders) => {


            res.status(200).json({
                success: true,
                totalItems: totalItems,
                orders: orders,
                currentPage: page,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE),
                hasPreviousPage: page > 1,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems

            })

        })




}