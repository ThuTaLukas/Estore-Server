const express = require('express');
const path = require('path');
const cors = require('cors')
// const passport = require('passport')


// *** express server initialization *** Start of Everything
const estoreAPI = express();


// backup server 
// const backupServer = express();


// getting  third party  and api middlewares and routes 
// app.use(passport.initialize())
// require('./services/passport');
require('./routes/index')(estoreAPI);
require('./routes/db')();



// // STATIC SERVER DIRECT to REACT FRONT-END IN PRODUCTION MODE . TO BUILD FOLDER 

// if(process.env.NODE_ENV === 'production') {


//     app.use(express.static('../client/build'))

//     app.get('*' , (req , res ) => {

//         res.sendFile(path.resolve(__dirname , 'client' , 'build' , 'index.html'));

//     });


// }


const PORT = 5000 || process.env.PORT;

estoreAPI.listen(process.env.PORT || 5001, () => {
    console.log('server is listening in port 5001')
})


// backupServer.listen(5002, () => {
//     console.log('backup server running in port 5002')
// })