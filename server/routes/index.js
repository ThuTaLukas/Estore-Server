const passport = require('passport');
const express = require('express');
const cookieParser = require('cookie-parser')
const cors = require('cors')
const config = require('../config/index.js')

// routes version 1
const userRoutes = require('./api/user')
const authRoutes = require('./api/auth')
const productRoutes = require('./api/products')
const cartRoutes = require('./api/cart')
const settingsRoutes = require('./api/settings');
const customerRoutes = require('./api/customers')
const adminRoutes = require('./api/admin')
const siteSettingsRoutes = require('./api/siteSettings/siteSettings')
const siteContentsRoutes = require('./api/siteContents')

module.exports = function (app) {
    // third party middlewares 
    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))


    const corsOptions = {
        origin: config.FRONTEND_URL,
        credentials: true
    }

    app.use(cors(corsOptions))
    app.use(passport.initialize())
    app.use(cookieParser());





    app.get('/', (req, res) => {

        res.json({ message: 'EStore BH API SERVER' })
    }
    )




    // API VERSION 1 

    app.use('/api/v1/users', userRoutes)
    app.use('/api/v1/delivery', settingsRoutes)
    app.use('/api/v1/sitesettings', siteSettingsRoutes)
    app.use('/api/v1/customers', customerRoutes)
    app.use('/api/v1/products', productRoutes)
    app.use('/api/v1/cart', cartRoutes)
    app.use('/api/v1/admin', adminRoutes)
    app.use('/api/v1/contents', siteContentsRoutes)
    // app.use('/auth', authRoutes)





}