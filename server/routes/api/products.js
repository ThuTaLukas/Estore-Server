const express = require('express');
const { authMiddleware } = require('../../middleware/auth')
const productsCtrl = require('../../controllers/products/products')
const cartsCtrl = require('../../controllers/products/carts')
const formidable = require('express-formidable')
const { CDphotoupload, CDimagedelete } = require('../../controllers/upload/photo')
const router = express.Router();




// IMAGE UPLOADS
router.post('/upload', authMiddleware, formidable(), CDphotoupload)
router.post('/upload/:productId/:imageId', authMiddleware, CDimagedelete)

// BRANDS
router.post('/brand', authMiddleware, productsCtrl.addBrand)
router.get('/brands', productsCtrl.getBrands)
router.post('/brand/delete/:brandId', authMiddleware, productsCtrl.deleteBrand)

// CATEGORIES
router.post('/category', authMiddleware, productsCtrl.addCategory)
router.get('/categories', productsCtrl.getCategories)
router.post('/categories/delete/:catId', authMiddleware, productsCtrl.deleteCategory)

// VARIANTS 
router.post('/variant', authMiddleware, productsCtrl.addVariant)
router.get('/variants', productsCtrl.getVariants)
router.post('/variants/delete/:variantId', authMiddleware, productsCtrl.deleteVariant)
// COLLECTIONS
router.post('/collection', authMiddleware, productsCtrl.addCollection)
router.get('/collections', productsCtrl.getCollections)
router.post('/collections/delete/:collectionId', authMiddleware, productsCtrl.deleteCollection)
// CARTS 

router.post('/cart', authMiddleware, cartsCtrl.addToCart)



// PRODUCTS
router.post('/like', productsCtrl.addLike)
router.get('/single/:productId', authMiddleware, productsCtrl.getSingleProduct)
router.post('/product', authMiddleware, productsCtrl.addProduct);
router.post('/all', authMiddleware, productsCtrl.getProducts)
router.post('/delete/:productId', authMiddleware, productsCtrl.deleteProduct)
router.post('/edit/:productId', authMiddleware, productsCtrl.editProduct)
router.post('/shop', productsCtrl.getProductsShop)
router.get('/getbycat/:catId', productsCtrl.getProductsByCat)


module.exports = router;