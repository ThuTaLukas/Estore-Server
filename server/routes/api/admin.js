const express = require('express');

const router = express.Router();
const formidable = require('express-formidable');
const photoUploadCtrl = require('../../controllers/upload/photo');
const { authMiddleware } = require('../../middleware/auth');
const adminCtrl = require('../../controllers/admin/admin');
const adminSettingsCtrl = require('../../controllers/admin/settings');
const adminPaymentsCtrl = require('../../controllers/admin/payment');


// USERS
router.post('/users', authMiddleware, adminCtrl.getUsers);
router.post('/users/delete/:userId', authMiddleware, adminCtrl.deleteUser);
router.get('/users/single/:userId', authMiddleware, adminCtrl.getSingleUser);
router.post('/users/edit', authMiddleware, adminCtrl.editUser);


// CUSTOMERS
router.post('/customers', authMiddleware, adminCtrl.getCustomers);


// ORDERS
router.get('/order/:id', authMiddleware, adminCtrl.getOrderById);

// PROMOTIONS
router.post('/promotion', authMiddleware, adminCtrl.addPromo);
router.post('/promotion/all', authMiddleware, adminCtrl.getPromotions);
router.post('/promotion/delete/:promoId', authMiddleware, adminCtrl.deletePromotion);
router.get('/promotion/single/:promoId', authMiddleware, adminCtrl.getSinglePromotion);
router.post('/promotion/edit', authMiddleware, adminCtrl.editPromotion);

// SETTINGS
router.post('/settings/info', authMiddleware, adminSettingsCtrl.CUStoreInfo);
router.get('/settings/info', authMiddleware, adminSettingsCtrl.getStoreProfile);

// SETTINGS PAYMENT
router.post('/settings/payment', authMiddleware, adminPaymentsCtrl.addPayment);

router.post('/upload/single', authMiddleware, formidable(), photoUploadCtrl.singleUpload);

module.exports = router;