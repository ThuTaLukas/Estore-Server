const express = require('express')
const router = express.Router()
const cartCtrl = require('../../controllers/products/carts')
const { authMiddleware } = require('../../middleware/auth')




router.post('/cart', cartCtrl.addToCart)
router.post('/cart/get', cartCtrl.getUserCart)
router.post('/cart/:cartId/:itemId', cartCtrl.removeCartItem)








module.exports = router;