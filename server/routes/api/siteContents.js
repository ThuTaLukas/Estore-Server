const express = require('express')
const router = express.Router()
const { authMiddleware } = require('../../middleware/auth')
const contentsCtrl = require('../../controllers/siteSettings/siteContents')



router.post('/slider', contentsCtrl.createSlider);
router.get('/slider', contentsCtrl.getSliders)
router.post('/faq', contentsCtrl.createFAQ)
router.get('/faq', contentsCtrl.getFAQS)
router.post('/slider/delete', contentsCtrl.deleteSlider)
router.post('/faq/delete', contentsCtrl.deleteFAQ)

module.exports = router;