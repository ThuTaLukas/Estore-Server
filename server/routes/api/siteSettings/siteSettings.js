const express = require('express')
const router = express.Router()
const { authMiddleware } = require('../../../middleware/auth')
const siteSettingsCtrl = require('../../../controllers/siteSettings/siteSettings')


router.get('/', siteSettingsCtrl.getSiteSettings);





module.exports = router;