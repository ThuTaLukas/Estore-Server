const express = require('express')
const router = express.Router()
const { authMiddleware } = require('../../middleware/auth')
const ordersCtrl = require('../../controllers/customers/orders')
const customersCtrl = require('../../controllers/customers/customers')

router.post('/orders/status', authMiddleware, ordersCtrl.changeStatus)
router.get('/orders/customer', authMiddleware, ordersCtrl.getOrdersByCustomer)
router.post('/orders/add', authMiddleware, ordersCtrl.addOrder)
router.post('/orders/get', authMiddleware, ordersCtrl.getOrders)
router.post('/enquiry', customersCtrl.sendEnquiry)
router.get('/enquiry', customersCtrl.getEnquires)



module.exports = router;