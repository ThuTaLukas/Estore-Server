const express = require('express')
const router = express.Router()
const { authMiddleware } = require('../../middleware/auth')
const deliveryCtrl = require('../../controllers/settings/delivery')



router.post('/add', authMiddleware, deliveryCtrl.addDelivery)
router.get('/get', authMiddleware, deliveryCtrl.getDelivery)
router.post('/delete', authMiddleware, deliveryCtrl.deleteDelivery)
router.post('/update', authMiddleware, deliveryCtrl.updateDelivery)
router.post('/edit', authMiddleware, deliveryCtrl.editDelivery)


module.exports = router;