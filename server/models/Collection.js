const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const collectionSchema = new Schema({


    name: {
        required: true,
        type: String,
        unique: 1,
        maxlength: 100
    },

    id: {
        required: true,
        type: String,
        unique: 1,
        maxlength: 100
    },
    image: {
        type: String
    },
    creator: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }



})



const Collection = mongoose.model('Collection', collectionSchema);
module.exports = {
    Collection,
    collectionSchema
}