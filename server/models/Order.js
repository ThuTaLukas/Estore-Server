const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const orderSchema = new Schema({


    status: {
        type: String,
        enum: ['DELIVERED', 'SHIPPING', 'INHOUSE'],
        default: 'INHOUSE'
    },

    cart: {},

    payment: {

        type: {
            type: String,
            enum: ['COD', 'PAID'],
            default: 'COD'
        },
        merchant: {

        },

        transactionData: {}
    },

    shippingAddress: {

        type: {
            type: String,
            enum: ['LOCAL', 'INTERNATIONAL']
        },
        name: {
            type: String
        },
        company: {
            type: String
        },
        email: {
            type: String
        },

        phone: {
            type: String
        },
        line1: {
            type: String
        },

        line2: {
            type: String
        },

        region: {
            type: String
        },
        country: {

            type: String,
            default: 'mm-myanmar'
        },

        zip: {
            type: String
        }
    },

    desireDate: {
        type: Date
    },

    orderedDate: {
        type: Date,
        default: Date.now()
    },
    localDelivery: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Delivery'

    },
    customer: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    }


})



const Order = mongoose.model('Order', orderSchema);
exports.Order = Order; 