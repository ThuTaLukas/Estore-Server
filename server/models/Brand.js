const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const brandSchema = new Schema({


    name: {
        required: true,
        type: String,
        unique: 1,
        maxlength: 100
    },

    id: {
        required: true,
        type: String,
        unique: 1,
        maxlength: 100
    },
    image: {
        type: String
    }, 
    creator: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }



})



const Brand = mongoose.model('Brand', brandSchema);
exports.Brand = Brand; 