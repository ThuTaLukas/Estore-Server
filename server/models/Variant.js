const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const variantSchema = new Schema({


    name: {

        type: String,
        maxlength: 100
    },
    id: {

        type: String,
        maxlength: 100
    },


    creator: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }



})



const Variant = mongoose.model('Variant', variantSchema);
module.exports = {
    Variant,
    variantSchema
}