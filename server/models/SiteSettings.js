const mongoose = require('mongoose');
const { paymentSchema } = require('./Payment')
const { currencySchema } = require('./Currency')

const siteSettingSchema = mongoose.Schema({

    currency: [currencySchema],
    payments: [paymentSchema],

    store: {
        name: {
            type: String
        },
        company: {
            type: String
        },
        email: {
            type: String
        },
        phone: {
            type: String
        },
        social: {
            facebook: {
                type: String
            },
            youtube: {
                type: String
            },
            instagram: {
                type: String
            }

        },
        address: {
            line1: {
                type: String
            },
            line2: {
                type: String
            },
            country: {
                type: String
            },
            region: {
                type: String
            },
            zip: {
                type: String
            }

        }
    },

    facebook: {

        chat: {
            pageId: {
                type: String
            },
            appId: {
                type: String
            }
        }

    },

    policies: {

        refunds: {
            type: String
        },
        returns: {
            type: String
        },
        products: {
            type: String
        },
        termsconds: {
            type: String
        }
    },

    siteId: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },







});

const SiteSettings = mongoose.model('SiteSettings', siteSettingSchema);

module.exports = { SiteSettings }