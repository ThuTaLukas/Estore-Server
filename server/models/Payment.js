const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const paymentSchema = new Schema({

    name: {
        type: String
    },
    type: {
        type: String
    },

    paymentType: {
        type: String
    },
    country: {
        type: String
    },

    keys: {
        private: String,
        public: String
    },

    qrcode: {},

    mobile: {
        type: String
    }


})



const Payment = mongoose.model('Payment', paymentSchema);
exports.Payment = Payment;
exports.paymentSchema = paymentSchema