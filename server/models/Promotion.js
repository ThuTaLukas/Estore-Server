const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const promotionSchema = new Schema({

    name: {
        type: String,
        maxlength: 10
    },

    percent: {
        type: Number
    },

    quantity: {
        type: Number
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }



})



const Promotion = mongoose.model('Promotion', promotionSchema);
exports.Promotion = Promotion;