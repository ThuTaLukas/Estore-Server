const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const deliverySchema = new Schema({

    township: {
        type: String,
        maxlength: 100

    },

    town: {
        type: String,
        maxlength: 100
    },

    price: {
        type: Number
    },

    creator: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    }



})



const Delivery = mongoose.model('Delivery', deliverySchema);
exports.Delivery = Delivery;