const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const productReviewSchema = new Schema({

    reviewer: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },

    rating: {
        type: Number
    },

    comment: {
        type: String
    }


})



const ProductReview = mongoose.model('ProductReview', productReviewSchema);
exports.ProductReview = ProductReview; 