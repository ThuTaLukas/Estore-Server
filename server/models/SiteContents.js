
const mongoose = require('mongoose');



const siteContentsSchema = mongoose.Schema({

    heading: {

        type: Array,
        default: []
    },
    headerImages: {
        type: Array,
        default: []
    },
    faqs: {
        type: Array,
        default: []
    },

    whys: {
        type: Array,
        default: []
    },

    about: {

    }



});

const SiteContents = mongoose.model('SiteContents', siteContentsSchema);

module.exports = { SiteContents }




