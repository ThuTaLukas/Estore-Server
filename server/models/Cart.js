const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const cartSchema = mongoose.Schema({

    totalPrice: {
        type: Number,
        default: 0
    },
    totalQuantity: {
        type: Number,
        default: 0
    },

    items: {
        type: Array,
        default: []
    },
    customer: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    }





})


const Cart = mongoose.model('Cart', cartSchema);
exports.Cart = Cart;
