const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const { collectionSchema } = require('./Collection')
const { variantSchema } = require('./Variant')




const productSchema = new Schema({

    name: {
        type: String
    },

    price: {
        promo: {
            type: Number
        },
        normal: {
            type: Number
        }
    },

    sku: {
        type: String

    },
    description: {
        type: String
    },

    details: {
        onsale: {
            type: Boolean
        },
        cod: {
            type: Boolean
        }
    },

    category: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Category'
    },
    brand: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Brand'
    },
    collections: [{
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Collection'
    }],
    variants: {},

    stocks: {
        type: Number,
        default: 0
    },

    likes: {
        type: Number,
        default: 0
    },
    images: {
        type: Array,
        default: []
    },

    reviews: [
        {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'ProductReview'
        }
    ],
    creator: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'User'
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }

})


productSchema.index({ '$**': 'text' })



const Product = mongoose.model('Product', productSchema);
exports.Product = Product;



