module.exports = {


  apps: [
    {
      name: 'estore-server',
      script: './server/server.js',
      // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
      env: {
        NODE_ENV: 'development',
        googleClientID: '',
        googleClientScret: ``,
        MONGO_URI: `mongodb://localhost:27017/estore`,
        stripePublishableKey: '',
        stripeSecretKey: ``,
        sendGridKey: ``,
        redirectDomain: ``,
        FACEBOOK_APP_ID: ``,
        FACEBOOK_APP_SECRET: ``,
        secretOrKey: ``,
        AWS_SECRET_KEY: ``,
        AWS_ACCESS_KEY_ID: ``,
        FRONTEND_URL: ``,
        ACCOUNTKIT_SECRET: ``,
        CD_CLOUD_NAME: ``,
        CD_CLOUD_API_KEY: ``,
        CD_CLOUD_API_SECRET: ``,
      },
      env_production: {
        NODE_ENV: 'production',
        NODE_ENV: 'development',
        googleClientID: '',
        googleClientScret: ``,
        MONGO_URI: `mongodb://localhost:27017/estore`,
        stripePublishableKey: '',
        stripeSecretKey: ``,
        sendGridKey: ``,
        redirectDomain: ``,
        FACEBOOK_APP_ID: ``,
        FACEBOOK_APP_SECRET: ``,
        secretOrKey: ``,
        AWS_SECRET_KEY: ``,
        AWS_ACCESS_KEY_ID: ``,
        FRONTEND_URL: ``,
        ACCOUNTKIT_SECRET: ``,
        CD_CLOUD_NAME: ``,
        CD_CLOUD_API_KEY: ``,
        CD_CLOUD_API_SECRET: ``,
      }
    }


  ],


};
